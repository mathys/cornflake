{ options, config, pkgs, lib, ... }:

with lib;
let cfg = config.cornflake.system.time;
in
{
  options.cornflake.system.time = with types; {
    enable =
      mkBoolOpt false "Whether or not to configure timezone information.";
  };

  config = mkIf cfg.enable { time.timeZone = "Europe/Brussels"; };
}
