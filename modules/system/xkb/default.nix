{ options, config, lib, ... }:

with lib;
let cfg = config.cornflake.system.xkb;
in
{
  options.cornflake.system.xkb = with types; {
    enable = mkBoolOpt false "Whether or not to configure xkb.";
  };

  config = mkIf cfg.enable {
    console.useXkbConfig = true;
    services.xserver = {
      layout = "us";
      xkbVariant = "alt-intl";
    };
  };
}

