{ options, config, lib, pkgs, ... }:

with lib;
let cfg = config.cornflake.system.network;
in
{
  options.cornflake.system.network = with types; {
    enable = mkBoolOpt false "Whether or not to enable network managment.";
  };

  config = mkIf cfg.enable {
    networking = {
      wireless.iwd.enable = true;

      #interfaces.wlan0.useDHCP = false;

      firewall.enable = false;

      networkmanager = {
        enable = true;
        wifi.backend = "iwd";
      };


    };
  };
}
