{ options, config, pkgs, lib, ... }:

with lib;
let cfg = config.cornflake.system.nix;
in
{
  options.cornflake.system.nix = with types; {
    enable = mkBoolOpt true "Whether or not to manage nix configuration.";
    package = mkOpt package pkgs.nix "Which nix package to use.";
  };

  config = mkIf cfg.enable {

    cornflake.home.extraOptions = {
      home.shellAliases = {
        cornflake-reinstall = ''su root -c 'cd /home/${config.cornflake.user.name}/.nix; rm -rdf base; curl "https://gitlab.com/mathys/cornflake/-/raw/main/install.sh?inline=false" | bash ' '';
        cornflake = ''su root -c 'cd /home/${config.cornflake.user.name}/.nix; curl "https://gitlab.com/mathys/cornflake/-/raw/main/update.sh?inline=false" | bash ' '';
      };
    };

    nix =
      let users = [ "root" config.cornflake.user.name ];
      in
      {
        package = cfg.package;
        extraOptions = lib.concatStringsSep "\n" [
          ''
            experimental-features = nix-command flakes
            http-connections = 50
            warn-dirty = false
            log-lines = 50
            sandbox = relaxed
          ''
        ];

        settings = {
          experimental-features = "nix-command flakes";
          http-connections = 50;
          warn-dirty = false;
          log-lines = 50;
          sandbox = "relaxed";
          auto-optimise-store = true;
          trusted-users = users;
          allowed-users = users;
          substituters = ["https://hyprland.cachix.org"];
          trusted-public-keys = ["hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="];
        };

        gc = {
          automatic = true;
          dates = "weekly";
          options = "--delete-older-than 30d";
        };

        # flake-utils-plus
        generateRegistryFromInputs = true;
        generateNixPathFromInputs = true;
        linkInputs = true;
      };
  };
}
