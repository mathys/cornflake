{ options, config, pkgs, lib, inputs, ... }:

with lib;
let cfg = config.cornflake.system;
in
{
  options.cornflake.system = with types; {
    enable = mkBoolOpt false "Whether or not to enable hardware handling.";
  };

  config = mkIf cfg.enable {
    cornflake.system = {
      audio = enabled;
      fonts = enabled;
      network = enabled;
      nix = enabled;
      time = enabled;
      xkb = enabled;
    };

    environment.systemPackages = with pkgs; [
      ntfs3g
      fuseiso
      pciutils
      usbutils
      acpi
      gcc
      appimage-run
      webkitgtk
      gnomeExtensions.appindicator
      gtk3
      cairo
      gdk-pixbuf
      glib
      dbus
      openssl_3
      librsvg
      libthai
      node2nix
      etcher
    ];
  };
}