{ options, config, lib, pkgs, ... }:

with lib;
let cfg = config.cornflake.programs.cli.git;
in
{
  options.cornflake.programs.cli.git = with types; {
    enable = mkBoolOpt false "Whether or not to enable git.";
  };

  config =
    mkIf cfg.enable { environment.systemPackages = with pkgs; [ git ]; };
}