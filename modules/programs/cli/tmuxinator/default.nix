{ options, config, lib, pkgs, ... }:

with lib;
let cfg = config.cornflake.programs.cli.tmuxinator;
in
{
  options.cornflake.programs.cli.tmuxinator = with types; {
    enable = mkBoolOpt false "Whether or not to enable tmuxinator.";
  };

  config =
    mkIf cfg.enable { environment.systemPackages = with pkgs; [ tmux tmuxinator ]; };
}
