{ options, config, lib, pkgs, ... }:

with lib;
let cfg = config.cornflake.programs.cli.docker;
in
{
  options.cornflake.programs.cli.docker = with types; {
    enable = mkBoolOpt false "Whether or not to enable docker.";
  };

  config = mkIf cfg.enable {
    virtualisation.docker.enable = true;
    users.users.${config.cornflake.user.name}.extraGroups = [ "docker" ];
  };
}