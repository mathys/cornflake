{ options, config, lib, pkgs, ... }:

with lib;

let cfg = config.cornflake.programs.cli.zsh;
in
{
  options.cornflake.programs.cli.zsh = with types; {
    enable = mkBoolOpt false "Whether or not to enable zsh.";
  };

  config = mkIf cfg.enable { 
    programs.zsh.enable = true;
    environment.systemPackages = with pkgs; [ oh-my-zsh ];
    users.users.${config.cornflake.user.name}.shell = pkgs.zsh;
    environment.pathsToLink = [ "/share/zsh" ];
    cornflake.home.extraOptions = hm: {
      programs.zsh = {
        enable = true;
        enableAutosuggestions = true;
        enableCompletion = true;
        enableSyntaxHighlighting = true;
        enableVteIntegration = true;
        autocd = true;
        plugins = [
          {
            name = "powerlevel10k";
            src = pkgs.zsh-powerlevel10k;
            file = "share/zsh-powerlevel10k/powerlevel10k.zsh-theme";
          }
          {
            name = "powerlevel10k-config";
            src = lib.cleanSource ./p10k-config;
            file = "p10k.zsh";
          }
        ];
        oh-my-zsh = {
          enable = true;
          custom = ".oh-my-zsh/custom";
        };
      };
    };
  };
}
