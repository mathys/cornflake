{ options, config, lib, pkgs, ... }:

with lib;
let cfg = config.cornflake.programs.cli.neovim;
in
{
  options.cornflake.programs.cli.neovim = with types; {
    enable = mkBoolOpt false "Whether or not to enable neovim.";
  };

  config = mkIf cfg.enable { 
    environment.systemPackages = with pkgs; [ neovim ripgrep xclip]; 
    cornflake.home.extraOptions = hm: {
      programs.neovim = {
        enable = true;
        coc.enable = true;
        extraLuaConfig = ''
        '';
        extraConfig = "";
        defaultEditor = true;
        extraPackages = [];
        plugins = [
          # pkgs.vimPlugins.tokyonight-nvim
          # pkgs.vimPlugins.coc-tabnine
          # pkgs.vimPlugins.vim-go
          # pkgs.vimPlugins.coc-go
          # pkgs.vimPlugins.rust-vim
          # pkgs.vimPlugins.coc-rust-analyzer
          # pkgs.vimPlugins.vim-jsx-typescript
          # pkgs.vimPlugins.vim-jsx-pretty
          # pkgs.vimPlugins.typescript-nvim
          # pkgs.vimPlugins.vim-javascript-syntax
          # pkgs.vimPlugins.vim-javascript
          # pkgs.vimPlugins.telescope-nvim
          pkgs.vimPlugins.nvchad-ui
          pkgs.vimPlugins.nvchad
          pkgs.vimPlugins.nvchad-extensions
        ];
        viAlias = true;
        vimdiffAlias = true;
        withNodeJs = true;
        withPython3 = true;
        withRuby = true;
      };
    };
  };
}
