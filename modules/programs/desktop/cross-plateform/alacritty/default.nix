{ options, config, lib, pkgs, ... }:

with lib;

let cfg = config.cornflake.programs.desktop.cross-plateform.alacritty;
in
{
  options.cornflake.programs.desktop.cross-plateform.alacritty = with types; {
    enable = mkBoolOpt false "Whether or not to enable alacritty.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ alacritty ];
    
    cornflake.home.extraOptions = {
      programs.alacritty = {
        enable = true;
        # settings = ./config/alacritty.yml;
      };  
    };
    environment.sessionVariables.TERMINAL = "alacritty";
  };
}
