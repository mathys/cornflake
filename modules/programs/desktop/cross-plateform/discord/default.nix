{ options, config, lib, pkgs, ... }:

with lib;

let cfg = config.cornflake.programs.desktop.cross-plateform.discord;
in
{
  options.cornflake.programs.desktop.cross-plateform.discord = with types; {
    enable = mkBoolOpt false "Whether or not to enable discord.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ discord ];
  };
}
