{ options, config, lib, pkgs, ... }:

with lib;

let cfg = config.cornflake.programs.desktop.cross-plateform.thunar;
in
{
  options.cornflake.programs.desktop.cross-plateform.thunar = with types; {
    enable = mkBoolOpt false "Whether or not to enable thunar.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ xfce.thunar ];
  };
}
