{ options, config, lib, pkgs, ... }:

with lib;

let cfg = config.cornflake.programs.desktop.cross-plateform.eww;
in
{
  options.cornflake.programs.desktop.cross-plateform.eww = with types; {
    enable = mkBoolOpt false "Whether or not to enable eww.";
  };

  config = mkIf cfg.enable { 
    environment.systemPackages = with pkgs; [ eww-wayland ]; 
    cornflake.home.extraOptions = hm: {
      home.file.".config/eww" = {
        source =
          hm.config.lib.file.mkOutOfStoreSymlink "/home/${config.cornflake.user.name}/.nix/base/modules/programs/desktop/cross-plateform/eww/config";
        recursive = true;
      };
    };
  };
}
