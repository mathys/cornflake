{ options, config, lib, pkgs, ... }:

with lib;

let cfg = config.cornflake.programs.desktop.cross-plateform.vscode;
in
{
  options.cornflake.programs.desktop.cross-plateform.vscode = with types; {
    enable = mkBoolOpt false "Whether or not to enable vscode.";
  };

  config = mkIf cfg.enable { 
    cornflake.home.extraOptions = hm: {

      programs.vscode = {
        enable = true;
        package = pkgs.vscodium;
        extensions = [
          pkgs.vscode-extensions.dracula-theme.theme-dracula
          pkgs.vscode-extensions.golang.go
          pkgs.vscode-extensions.rust-lang.rust-analyzer
          pkgs.vscode-extensions.jakebecker.elixir-ls
          pkgs.vscode-extensions.tabnine.tabnine-vscode
          pkgs.vscode-extensions.ms-azuretools.vscode-docker
          pkgs.vscode-extensions.mads-hartmann.bash-ide-vscode
          pkgs.vscode-extensions.bbenoist.nix
          pkgs.vscode-extensions.pkief.material-icon-theme
        ];
      };

      home.file.".config/VSCodium/User/settings.json" = {
        source =
          hm.config.lib.file.mkOutOfStoreSymlink "/home/${config.cornflake.user.name}/.nix/base/modules/programs/desktop/cross-plateform/vscode/config";
      };

    };

  };
}
