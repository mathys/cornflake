{ options, config, lib, pkgs, ... }:

with lib;

let cfg = config.cornflake.programs.desktop.cross-plateform.networkmanagerapplet;
in
{
  options.cornflake.programs.desktop.cross-plateform.networkmanagerapplet = with types; {
    enable = mkBoolOpt false "Whether or not to enable networkmanagerapplet.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ networkmanagerapplet ];
  };
}
