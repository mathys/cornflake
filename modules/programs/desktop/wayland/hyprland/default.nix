{ options, config, lib, pkgs, ... }:

with lib;
let cfg = config.cornflake.programs.desktop.wayland.hyprland;
in
{
  options.cornflake.programs.desktop.wayland.hyprland = with types; {
    enable = mkBoolOpt false "Whether or not to enable Hyprland.";
  };

  config = mkIf cfg.enable {

    environment.systemPackages = with pkgs; [ hyprpaper ];

    programs.hyprland = {
      enable = true;
      xwayland = {
        enable = true;
        hidpi = false;
      };
      nvidiaPatches = false;
    };

    cornflake.home.configFile."electron-flags.conf".source =
      ./electron-flags.conf;

    environment.sessionVariables = {
      NIXOS_OZONE_WL = "1";
    };

    services.dbus.enable = true;

    cornflake.home.extraOptions = hm: {
      home.file.".config/hypr" = {
        source =
          hm.config.lib.file.mkOutOfStoreSymlink "/home/${config.cornflake.user.name}/.nix/base/modules/programs/desktop/wayland/hyprland/hypr";
        recursive = true;
      };
      home.file."Pictures/wallpaper.png" = {
        source =
          hm.config.lib.file.mkOutOfStoreSymlink "/home/${config.cornflake.user.name}/.nix/base/modules/programs/desktop/wayland/hyprland/wallpaper.png";
      };
    };
    services.xserver.enable = true;
    services.xserver.displayManager.autoLogin.user = config.cornflake.user.name;
    services.xserver.displayManager.defaultSession = "hyprland";
  };
}