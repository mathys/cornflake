{ options, config, lib, pkgs, ... }:

with lib;

let cfg = config.cornflake.programs.desktop.wayland.wofi;
in
{
  options.cornflake.programs.desktop.wayland.wofi = with types; {
    enable =
      mkBoolOpt false "Whether to enable the Wofi in the desktop environment.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ wofi wofi-emoji ];

    # config -> .config/wofi/config
    # css -> .config/wofi/style.css
    # colors -> $XDG_CACHE_HOME/wal/colors
    # cornflake.home.configFile."foot/foot.ini".source = ./foot.ini;
    cornflake.home.configFile."wofi/config".source = ./config;
    cornflake.home.configFile."wofi/style.css".source = ./style.css;
  };
}
