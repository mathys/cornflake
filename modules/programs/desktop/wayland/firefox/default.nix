{ options, config, lib, pkgs, ... }:

with lib;

let
  cfg = config.cornflake.programs.desktop.wayland.firefox;
  sweet_pop_source = builtins.fetchTarball {
    url    = "https://github.com/PROxZIMA/Sweet-Pop/archive/refs/tags/4.0.1.tar.gz";
    sha256 = "0kqzwnlk4ar2cnl5z1jkf570pyjy4ynxr07v6k6h7j1z95a7sz1m";
  };
in
{
  options.cornflake.programs.desktop.wayland.firefox = with types; {
    enable = mkBoolOpt false "Whether or not to enable Firefox.";
  };

  config = mkIf cfg.enable {

    cornflake.home.extraOptions = {
      home.file."sweet_pop" = {
        target = ".mozilla/firefox/default/chrome/sweet_pop";
        source = sweet_pop_source;
      };
      programs.firefox = {
        enable = true;
        profiles.${config.cornflake.user.name} = {
          id = 0;
          name = config.cornflake.user.name;
          userChrome = ''
            @import "sweet_pop/userChrome.css";
            @import "sweet_pop/userContent.css"; 
            @import "sweet_pop/floatingToolbox.css";
          '';
          settings = {
            "browser.aboutwelcome.enabled" = false;
            "browser.meta_refresh_when_inactive.disabled" = true;
            "browser.startup.homepage" = "https://start.duckduckgo.com/?kak=-1&kal=-1&kao=-1&kaq=-1&kt=Hack+Nerd+Font&kae=d&ks=m&k7=2e3440&kj=3b4252&k9=eceff4&kaa=d8dee9&ku=1&k8=d8dee9&kx=81a1c1&k21=3b4252&k18=1&k5=2&kp=-2&k1=-1&kaj=u&kay=b&kk=-1&kax=-1&kap=-1&kau=-1";
            "browser.bookmarks.showMobileBookmarks" = true;
            "browser.urlbar.suggest.quicksuggest.sponsored" = false;
            "browser.newtabpage.activity-stream.showSponsoredTopSites" = false;
            "browser.aboutConfig.showWarning" = false;
            "browser.ssb.enabled" = true;
            "extensions.activeThemeID" = "firefox-compact-dark@mozilla.org";
          };
        };
      };
    };
  };
}
