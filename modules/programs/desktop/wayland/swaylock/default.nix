{ options, config, lib, pkgs, ... }:

with lib;
let cfg = config.cornflake.programs.desktop.wayland.swaylock;
in
{
  options.cornflake.programs.desktop.wayland.swaylock = with types; {
    enable = mkBoolOpt false "Whether or not to enable swaylock.";
  };

  config = mkIf cfg.enable { 
    environment.systemPackages = with pkgs; [ swaylock ]; 
    security.pam.services.swaylock = {
      text = "auth include login";
    };
  };
}