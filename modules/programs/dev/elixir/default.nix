{ options, config, pkgs, lib, ... }:

with lib;
let
  cfg = config.cornflake.programs.dev.elixir;
in
{
  options.cornflake.programs.dev.elixir = with types; {
    enable = mkBoolOpt false "Whether or not to enable Elixir support.";
  };

  config = mkIf cfg.enable {
    environment = {
      systemPackages = with pkgs; [ elixir ];
    };
  };
}
