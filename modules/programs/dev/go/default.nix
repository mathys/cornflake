{ options, config, pkgs, lib, ... }:

with lib;
let
  cfg = config.cornflake.programs.dev.go;
in
{
  options.cornflake.programs.dev.go = with types; {
    enable = mkBoolOpt false "Whether or not to enable Go support.";
  };

  config = mkIf cfg.enable {
    environment = {
      systemPackages = with pkgs; [ go gopls ];
      sessionVariables = {
        GOPATH = "$HOME/work/go";
      };
    };
  };
}

