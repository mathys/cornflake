{ options, config, pkgs, lib, ... }:

with lib;
let
  cfg = config.cornflake.programs.dev.python;
in
{
  options.cornflake.programs.dev.python = with types; {
    enable = mkBoolOpt false "Whether or not to enable python support.";
  };

  config = mkIf cfg.enable {
    environment = {
      systemPackages = with pkgs; [
        python311Packages.pip
	python311
      ];
    };
  };
}
