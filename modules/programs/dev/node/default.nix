{ options, config, pkgs, lib, ... }:

with lib;
let cfg = config.cornflake.programs.dev.node;
in
{
  options.cornflake.programs.dev.node = with types; {
    enable = mkBoolOpt false "Whether or not to install and configure git";
    pkg = mkOpt package pkgs.nodejs-18_x "The NodeJS package to use";
    prettier = {
      enable = mkBoolOpt true "Whether or not to install Prettier";
      pkg =
        mkOpt package pkgs.nodePackages.prettier "The NodeJS package to use";
    };
    yarn = {
      enable = mkBoolOpt true "Whether or not to install Yarn";
      pkg = mkOpt package pkgs.nodePackages.yarn "The NodeJS package to use";
    };
    expo-cli = {
      enable = mkBoolOpt true "Whether or not to install Expo cli";
      pkg = mkOpt package pkgs.nodePackages.expo-cli "The Expo cli package to use";
    };
    npm = {
      enable = mkBoolOpt true "Whether or not to install npm";
      pkg = mkOpt package pkgs.nodePackages.npm "The npm package to use";
    };

    tree-sitter-cli = {
      enable = mkBoolOpt true "Whether or not tree sitter cli";
      pkg = mkOpt package pkgs.cornflake.tree-sitter-cli "The tree sitter cli package to use";
    };

  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs;
      [ cfg.pkg ] ++ (lib.optional cfg.prettier.enable cfg.prettier.pkg)
      ++ (lib.optional cfg.yarn.enable cfg.yarn.pkg)
      ++ (lib.optional cfg.expo-cli.enable cfg.expo-cli.pkg)
      ++ (lib.optional cfg.npm.enable cfg.npm.pkg);
  };
}