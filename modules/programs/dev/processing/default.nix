{ options, config, pkgs, lib, ... }:

with lib;
let
  cfg = config.cornflake.programs.dev.processing;
in
{
  options.cornflake.programs.dev.processing = with types; {
    enable = mkBoolOpt false "Whether or not to enable processing support.";
  };

  config = mkIf cfg.enable {
    environment = {
      systemPackages = with pkgs; [
        processing
      ];
    };
  };
}