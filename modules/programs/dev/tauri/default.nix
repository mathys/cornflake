{ options, config, lib, pkgs, ... }:

with lib;
let
  cfg = config.cornflake.programs.dev.tauri;
in
{
  options.cornflake.programs.dev.tauri = with types; {
    enable = mkBoolOpt false "Whether or not to enable tauri";
  };

  config = mkIf cfg.enable { environment.systemPackages = with pkgs.cornflake; [ tauri ]; };
}