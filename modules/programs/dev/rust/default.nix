{ options, config, pkgs, lib, ... }:

with lib;
let
  cfg = config.cornflake.programs.dev.rust;
in
{
  options.cornflake.programs.dev.rust = with types; {
    enable = mkBoolOpt false "Whether or not to enable Rust support.";
  };

  config = mkIf cfg.enable {
    environment = {
      systemPackages = with pkgs; [ 
        cargo 
        rustc
      ];
    };
  };
}

