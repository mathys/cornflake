{ options, config, pkgs, lib, inputs, ... }:

with lib;
let
  cfg = config.cornflake.user;
in
{
    imports = with inputs; [
        home-manager.nixosModules.home-manager
    ];

    options.cornflake.user = with types; {
        name = mkOpt str "ricecooker" "The name to use for the user account.";
        fullName = mkOpt str "Rice Cooker" "The full name of the user.";
        email = mkOpt str "mathys.dupont.37@gmail.com" "The email of the user.";
        initialPassword = mkOpt str "pass" "The initial password to use when the user is first created.";
        extraGroups = mkOpt (listOf str) [ ] "Groups for the user to be assigned.";
        extraOptions = mkOpt attrs { } "Extra options passed to <option>users.users.<name></option>.";
    };

    options.cornflake.home = with types; {
        configFile = mkOpt attrs { } "A set of files to be managed by home-manager's <option>xdg.configFile</option>.";
        extraOptions = mkOpt attrs { } "Options to pass directly to home-manager.";
    };


    config = {

        cornflake.home.extraOptions = {
            home.stateVersion = config.system.stateVersion;
            xdg.enable = true;
            xdg.configFile = mkAliasDefinitions options.cornflake.home.configFile;
            home.file = {
                "Desktop/.keep".text = "";
                "Documents/.keep".text = "";
                "Downloads/.keep".text = "";
                "Music/.keep".text = "";
                "Pictures/.keep".text = "";
                "Videos/.keep".text = "";
                "Sandbox/.keep".text = "";
            };
        };

        home-manager = {
            useUserPackages = true;
            users.${config.cornflake.user.name} = mkAliasDefinitions options.cornflake.home.extraOptions;
        };

        users.users.${cfg.name} = {
            isNormalUser = true;

            inherit (cfg) name initialPassword;


            home = "/home/${cfg.name}";
            group = "users";

            uid = 8888;

            extraGroups = [ "wheel" ] ++ cfg.extraGroups;


        } // cfg.extraOptions;
    };
}