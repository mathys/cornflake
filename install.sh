#!/bin/sh

function pull(){
    nix-env -iA nixos.unzip
    curl -L https://gitlab.com/mathys/cornflake/-/archive/main/cornflake-main.zip --output flake.zip && unzip flake.zip -d . #.
    rm flake.zip
    mv ./cornflake-main ./base
}

function garbageCollect(){
    sudo nix-collect-garbage --delete-older-than 10d
}

function install(){
    nix registry add my-flakes ~/flakes/base --extra-experimental-features nix-command --extra-experimental-features flakes
    nix flake update ~/flakes/base --extra-experimental-features nix-command --extra-experimental-features flakes
    (cd base; nixos-rebuild switch --flake .#wcooker --show-trace --impure)
}

garbageCollect
pull
install
