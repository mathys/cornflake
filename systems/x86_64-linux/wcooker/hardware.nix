{ config, pkgs, lib, modulesPath, inputs, ... }:

let
  inherit (inputs) nixos-hardware;
in
{
  imports = with nixos-hardware.nixosModules; [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    kernelModules = [ "kvm-intel" ];

    loader.systemd-boot.enable = true;
    loader.efi.canTouchEfiVariables = true;

    initrd = {
      availableKernelModules =
        [ "nvme" "ahci" "xhci_pci" "usbhid" "usb_storage" "sd_mod" ];
    };

    extraModulePackages = [ ];
  };

  fileSystems."/" = {
    device = "/dev/nvme0n1p3";
    fsType = "ext4";
  };

  fileSystems."/boot" = {
    device = "/dev/nvme0n1p1";
    fsType = "vfat";
  };

  swapDevices = [
    { device = "/dev/nvme0n1p2"; }
  ];

  hardware.enableRedistributableFirmware = true;

  hardware.bluetooth.enable = true;
}
