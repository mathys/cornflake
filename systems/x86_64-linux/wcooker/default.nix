{ config, pkgs, lib, nixos-hardware, ... }:

with lib;

{
  imports = [ ./hardware.nix ];

  cornflake = {
    system = enabled;
    programs = {
      desktop = {
        wayland = {
          firefox = enabled;
          hyprland = enabled;
          wofi = enabled;
          swaylock = enabled;
        };
        cross-plateform = {
          alacritty = enabled;
          eww = enabled;
          vscode = enabled;
          networkmanagerapplet = enabled;
          discord = enabled;
          thunar = enabled;
        };
      };
      cli = {
        git = enabled;
        zsh = enabled;
        docker = enabled;
	      neovim = enabled;
        tmuxinator = enabled;
        helix = enabled;
      };
      dev = {
        elixir = enabled;
        go = enabled;
        node = enabled;
        rust = enabled;
        processing = enabled;
	      python = enabled;
      };
    };
  };

  system.stateVersion = "23.05";
}
