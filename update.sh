#!/bin/sh

function gitRepository() {
    if ! [ -d "base/.git" ]; then
        rm -rdf base
        git clone https://gitlab.com/mathys/cornflake.git base
        cd base
        nix registry add my-flakes ~/flakes/base --extra-experimental-features nix-command --extra-experimental-features flakes
        nix flake update ~/flakes/base --extra-experimental-features nix-command --extra-experimental-features flakes
    fi
}

function pull(){
    cd base
    git pull --rebase
}

function update(){
    cd base
    mv .git .idontevenknowwhatimdoing.git
    mv .gitignore .idontevenknowwhatimdoing.gitignore
    nix flake update ~/flakes/base --extra-experimental-features nix-command --extra-experimental-features flakes
    nixos-rebuild switch --flake .#wcooker --show-trace --impure
    mv .idontevenknowwhatimdoing.git .git 
    mv .idontevenknowwhatimdoing.gitignore .gitignore
}

gitRepository
pull
update